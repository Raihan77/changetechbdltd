<?php

namespace App\Http\Controllers;

use App\Album;
use Illuminate\Http\Request;

class AlbumController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $albums = Album::all();
        return view('albums.index',compact('albums',$albums));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('albums.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $allData = $request->all();
            if($request->hasFile('picture')){
                $fileName = time().'-'.$request->picture->getClientOriginalName();
                $request->picture->move(public_path('pictures/albums'), $fileName);
                $allData['picture'] = $fileName;
            }else{
                $allData['picture'] = null;
            }
            Album::create($allData);
            return redirect()->route('albums.index')->with('message', 'Data has been inserted successfully');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function show(Album $album)
    {
        return view('albums.show',compact('album',$album));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function edit(Album $album)
    {
        return view('albums.edit',compact('album',$album));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Album $album)
    {
        try{
            $allData = $request->all();
            $oldImage = $album->picture;
            if($request->picture){
                $fileName= time().'_'.$request->picture->getClientOriginalName();
                $request->picture->move(public_path('/pictures/albums/'), $fileName);
                $allData['picture'] = $fileName;
                if(file_exists(public_path("/pictures/albums/$oldImage"))){
                    unlink(public_path("/pictures/albums/$oldImage"));
                }
            }
            else{
                $allData['picture']= $oldImage;
            }
            $album->update($allData);
            return redirect()->route('albums.index')->with('message', "Data has been updated successfully");
        }catch(QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function destroy(Album $album)
    {
        try{

            $oldImage = $album->picture;
            if(file_exists(public_path("/pictures/albums/$oldImage"))){
                unlink(public_path("/pictures/albums/$oldImage"));
            }

            $album->delete($oldImage);

            return redirect()->route('albums.index')->with('message', 'Data has been deleted successfully');
        }catch(QueryException $e){
            return redirect()->route('albums.index')->withErrors($e->getMessage());
        }
    }
}
