@extends('layout.mainlayout')

@section('content')
    <div class="container mb-4">
        <h4>Create Album</h4>
        <hr>

        {!! Form::open(array('url' => 'albums','method' => 'POST', 'enctype'=>'multipart/form-data')) !!}

        <div class="form-group row">
            {{Form::label('title', 'Picture Title', ['class'=>'col-sm-3 col-form-label'])}}
            <div class="col-sm-9">
                {{Form::text('title', old('title') ? old('title') : (!empty($product) ? $product->title : null),['class'=>'form-control','placeholder' => 'Enter Title Here'])}}
            </div>
        </div>

        <div class="form-group row">
            {{Form::label('picture', "Picture", ['class'=>'col-sm-3 col-form-label'])}}
            <div class="col-sm-9">

                {{Form::file("picture",['class'=>'form-control-file', 'accept'=>'image/*'])}}
            </div>
        </div>

        <div class="form-group row">
            {{Form::label('description', 'Description', ['class'=>'col-sm-3 col-form-label'])}}
            <div class="col-sm-9">
                {{Form::text('description',old('description') ? old('description') : (!empty($product) ? $product->description : null),['class'=>'form-control','placeholder'=>'Type here...'])}}
            </div>
        </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {{--<button type="submit" class="btn btn-primary">Submit</button>--}}

        {{Form::submit('Submit', ['class'=>'btn btn-primary mr-2'])}}

        <a href="{{route('albums.index')}}" class="btn btn-light"> Cancel </a>

        {!! Form::close() !!}

    </div>

@endsection