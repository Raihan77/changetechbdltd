@extends('layout.mainlayout')

@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title text-center"> Show Picture Details</h4>

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <tbody>
                            <tr class="bg-light">
                                <td> <strong> Field Name </strong></td>
                                <td> <strong> Details </strong></td>
                            </tr>


                            <tr>
                                <td> Title </td>
                                <td> {{$album->title}} </td>
                            </tr>
                            <tr>
                                <td> Picture </td>
                                <td><img width="200px" height="150px" src="{{asset('/pictures/albums/'.$album->picture)}}" alt=""> </td>
                            </tr>

                            <tr>
                                <td> Description </td>
                                <td> {{$album->description}} </td>
                            </tr>


                            </tbody>
                        </table>
                    </div> <!-- end table-responsive -->
                    <br>
                    <a href="{{route('albums.index')}}" > &laquo; Go Back </a>
                </div> <!-- end card body -->
            </div> <!-- card -->
        </div><!-- end col-lg-12 grid-margin stretch-card -->
    </div> <!-- end row -->
@endsection