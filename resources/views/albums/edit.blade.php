@extends('layout.mainlayout')

@section('content')

    <div class="col-10 offset-1 grid-margin stretch-card mb-2">

        <div class="card">
            <div class="card-header">
                <h4 class="card-title text-center"> Edit Album</h4>

                @if($errors->any())
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger"> {{$error}} </div>
                    @endforeach
                @endif
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                {!! Form::open(array('url' => 'albums/'.$album->id,'method' => 'put', 'enctype'=>'multipart/form-data')) !!}


                <div class="form-group row">
                    {{Form::label('title', 'Title', ['class'=>'col-sm-3 col-form-label'])}}
                    <div class="col-sm-9">
                        {{Form::text('title', old('title') ? old('title') : (!empty($album) ? $album->title : null),['class'=>'form-control','placeholder' => 'Enter Title Here'])}}
                    </div>
                </div>
                <div class="form-group row">
                    {{Form::label("picture", "Picture", ['class'=>'col-sm-3 col-form-label'])}}
                    <div class="col-sm-9">
                        {{Form::file("picture",['class' => 'form-control-file','accept'=>'image/*'])}}
                        <img width="100px" height="80px" src="{{asset("/pictures/albums/".$album->picture)}}" alt="Not Found">
                    </div>
                </div>

                <div class="form-group row">
                    {{Form::label('description', 'Description', ['class'=>'col-sm-3 col-form-label'])}}
                    <div class="col-sm-9">
                        {{Form::text('description',old('description') ? old('description') : (!empty($album) ? $album->description : null),['class'=>'form-control','placeholder'=>'Type here...'])}}
                    </div>
                </div>


                {{Form::submit('Update', ['class'=>'btn btn-primary mr-2'])}}

                <a href="{{route('albums.index')}}" class="btn btn-light"> Cancel </a>

                {!! Form::close() !!}

            </div> <!-- card-body -->
        </div> <!-- end card -->
    </div>


@endsection