@extends('layout.mainlayout')

@section('content')
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <div class="container mb-2">

        <a class="btn btn-secondary easyAccess mt-1 mb-1" href="{{route('albums.create')}}">Add New Photo </a>
        <a class="btn btn-secondary easyAccess float-right mt-1 mb-1" href="{{route('home')}}">Back to Dashboard </a>
        <table class="table">
            <thead class="thead-dark">
<h5 class="text-center">The CRUD Album</h5>
            <tr>
                <th scope="col">Sl.</th>
                <th scope="col">Title</th>
                <th scope="col">Picture</th>
                <th scope="col">Description</th>
                <th scope="col">Created At</th>
                <th scope="col">Modified At</th>
                <th scope="col" class="text-center">Action</th>
            </tr>
            </thead>
            <tbody>
            @php $sl=1 @endphp
            @foreach($albums as $album)
                <tr>
                    <td>{{$sl++}}</td>

                    <td> {{$album->title}} </td>
                    <td>
                        @if(!empty($album->picture))
                            <img width="100px" height="80px" src="{{asset("/pictures/albums/".$album->picture)}}" alt="Not Found"> </td>
                    @else
                        <strong> {{"Empty Image"}} </strong>
                    @endif

                    <td>{{$album->description}}</td>

                    <td>{{$album->created_at->toFormattedDateString()}}</td>

                    <td>{{$album->updated_at->toFormattedDateString()}}</td>

                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">

                            <a href="{{ URL::to('albums/' . $album->id . '/edit') }}">
                                <button type="button" class="btn btn-warning mr-1">Edit</button>
                            </a>

                            <a href="{{route('albums.show', $album->id)}}">
                                <button type="button" class="btn btn-success mr-1">Show</button>
                            </a>

                            {!! Form::open(array('url' => route('albums.destroy', $album->id),'method' => 'delete', 'class' => 'deleteForm pull-left')) !!}
                            {{ Form::button('Delete', ['type' => 'submit', 'class' => 'btn btn-danger'])}}
                            {!! Form::close() !!}

                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

@endsection