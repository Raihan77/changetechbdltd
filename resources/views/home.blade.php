@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!

                        <a class="btn btn-info easyAccess" style="float:right" href="{{url('albums')}}">Go to Album </a>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
