<div class="collapse bg-inverse" id="navbarHeader">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 mt-2">
                <h4 class="text-white">About ChangeTech</h4>
                <p class="text-muted">Most Leading IT Company in the country, based on Chattogram.</p>
            </div>
            <div class="col-sm-4 mt-2">
                <h4 class="text-white">Contact</h4>
                <ul class="list-unstyled">

                    <li><a href="#" class="text-white">Like on Facebook</a> | <a href="#" class="text-white">Email Us</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="navbar navbar-inverse bg-inverse">
    <div class="container d-flex justify-content-between">
        <a href="#" class="navbar-brand">ChangeTech <img src="/pictures/icon.png" style="width:40px; height: 40px;" alt="pic"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
</div>